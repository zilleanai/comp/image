# image

Image component

## Installation

```yml
# zillean-domain.yml

name: domain
comps:
 - https://gitlab.com/zilleanai/comp/image.git
```

```py
# unchained_config.py

BUNDLES = [
    'flask_unchained.bundles.api',
...
    'bundles.project',
    'bundles.image',
...
    'backend',  # your app bundle *must* be last
]
```

```py
# routes.py

routes = lambda: [
    include('bundles.project.routes'),
...
    include('bundles.image.routes'),
...
    controller(SiteController), 
]
```

```js
// routes.js
import {
  ImageUpload,
} from 'comps/image/pages'
...
export const ROUTES = {
  Home: 'Home',
  ...
  ImageUpload: 'ImageUpload',
  ...
}
...
const routes = [
  {
    key: ROUTES.Home,
    path: '/',
    component: Home,
  },
  ...
  {
    key: ROUTES.ImageUpload,
    path: '/imageupload/:subpath',
    component: ImageUpload,
  },
  ...
]
```

```js
// NavBar.js
<div className="menu left">
    <NavLink to={ROUTES.Projects} />
    ...
    <NavLink to={ROUTES.ImageUpload} params={ { subpath: '/' }} />
    ...
</div>
```
