import { createRoutine } from 'actions'
export const listImages = createRoutine('image/LIST_IMAGES')
export const download = createRoutine('image/DOWNLOAD_FILE')