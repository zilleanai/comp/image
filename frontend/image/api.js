import { get, post } from 'utils/request'
import { storage } from 'comps/project'
import { v1 } from 'api'

function image(uri) {
    return v1(`/image${uri}`)
}

export default class Image {
    static listImages(payload) {
        return get(image(`/list/${storage.getProject()}${payload.path}`))
    }
    /**
    * @param {Object} project
    */
    static download(id) {
        return get(image(`/download/${id}`))
    }

    static thumbnail(id) {
        return get(image(`/thumbnail/${storage.getProject()}/${id}`))
    }
}
