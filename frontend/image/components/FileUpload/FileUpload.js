import React from 'react'
import { compose } from 'redux'
import './file-upload.scss'

import { FilePond, File, registerPlugin } from 'react-filepond';
import 'filepond/dist/filepond.min.css';
import FilePondPluginImagePreview from 'filepond-plugin-image-preview';
import 'filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css';
import { storage } from 'comps/project'

registerPlugin(FilePondPluginImagePreview);

class FileUpload extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      // Set initial files
      files: []
    };
  }

  handleInit() {
  }

  render() {
    const { url, path, tags } = this.props
    return (

      <FilePond ref={ref => this.pond = ref}
        allowMultiple={true}
        maxFiles={100}
        server={`${url}/${storage.getProject()}${path == '/' ? '' : path}/${tags}`}
        oninit={() => this.handleInit()}
        onupdatefiles={(fileItems) => {
          // Set current file objects to this.state
          this.setState({
            files: fileItems.map(fileItem => fileItem.file)
          });
        }}>

        {/* Update current files  */}
        {this.state.files.map(file => (
          <File key={file} src={file} origin="local" />
        ))}

      </FilePond>

    );
  }
}

export default compose(
)(FileUpload)
