import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import classnames from 'classnames'
import { bindRoutineCreators } from 'actions'
import { injectReducer, injectSagas } from 'utils/async'
import './image.scss'
import { storage } from 'comps/project'
import { v1 } from 'api'
import { TagSelect } from 'comps/tag/components'
import { listFileTags, tagFile } from 'comps/file/actions'
import { selectFileTagsList } from 'comps/file/reducers/filetags'

class Image extends React.Component {

  constructor(props) {
    super(props);
    this.state = {};
    this.onClick = this.onClick.bind(this);
    this.handleTagChange = this.handleTagChange.bind(this)
  }

  componentWillMount() {
    const { image, path } = this.props
    this.props.listFileTags.maybeTrigger({ project: storage.getProject(), path, filename: image })
  }

  componentWillReceiveProps(nextProps) {
    const { listFileTags, image, path, tags } = nextProps
    if (image != this.props.image) {
      listFileTags.trigger({ project: storage.getProject(), path, filename: image })
    }
    if (nextProps.isLoaded) {
      this.setState({
        image,
        tags
      });
    }
  }

  handleTagChange({ value }) {
    const { tagFile, image, path, listFileTags } = this.props
    tagFile.trigger({ project: storage.getProject(), path, 'filename': image, 'tags': value })
    listFileTags.trigger({ project: storage.getProject(), path, filename: image })
    this.setState({
      image,
      tags: value
    });
  }

  onClick() {
    const { image, path } = this.props
    this.props.listFileTags.maybeTrigger({ project: storage.getProject(), path, filename: image })
    this.setState({ selected: !this.state.selected });
  }

  render() {
    const { isLoaded, error, path, image, tags } = this.props
    const { selected } = this.state
    if (!isLoaded) {
      return null
    }
    return (
      selected ? <div className='selected-image'>
        <p>{image}</p>
        <img onClick={this.onClick} src={v1(`/image/thumbnail/${storage.getProject()}${path}/${image}`)} />

        <TagSelect defaultValue={tags} onChange={this.handleTagChange} />
        <p>
          <a href={v1(`/image/download/${storage.getProject()}${path}/${image}`)} download={image}><button>Download!</button></a>
        </p>
      </div> :
        <div onClick={this.onClick} className='image'><img src={v1(`/image/thumbnail/${storage.getProject()}${path}/${image}`)} /></div>
    )
  }
}

const withReducer = injectReducer(require('comps/file/reducers/filetags'))
const withSaga = injectSagas(require('comps/file/sagas/filetags'))
const withSaga2 = injectSagas(require('comps/file/sagas/tagFile'))

const withConnect = connect(
  (state, props) => {
    let path = '/' + props.path
    const image = props.image
    const project = storage.getProject()
    const tags = selectFileTagsList(state, project, path, image)
    const isLoaded = !state.selected || !!tags
    return {
      isLoaded,
      image,
      tags,
      selected: false
    }
  },
  (dispatch) => bindRoutineCreators({ listFileTags, tagFile }, dispatch),
)

export default compose(
  withReducer,
  withSaga,
  withSaga2,
  withConnect,
)(Image)
