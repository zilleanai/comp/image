import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import classnames from 'classnames'
import { bindRoutineCreators } from 'actions'
import { injectReducer, injectSagas } from 'utils/async'
import { listImages } from 'comps/image/actions'
import { selectImagesList } from 'comps/image/reducers/images'
import { storage } from 'comps/project'
import { Image } from 'comps/image/components'
import './image-list.scss'

class ImageList extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
    }
  }

  componentWillMount() {
    const { path, listImages } = this.props
    listImages.maybeTrigger({
      project: storage.getProject(), path
    })
  }

  componentWillReceiveProps(nextProps) {
    const { path, listImages } = nextProps
    listImages.maybeTrigger({
      project: storage.getProject(), path
    })
  }

  render() {
    const { path, images } = this.props
    if (!images || images.length === 0) {
      return (<p>No images.</p>)
    }
    return (
      <div className='imagelist'>
        {images.map((image, i) => {
          return (
            <Image key={`image-${i}`} path={path == '/' ? '' : path} image={image} />
          )
        }
        )}
      </div>
    )
  }
}

const withReducer = injectReducer(require('comps/image/reducers/images'))
const withSaga = injectSagas(require('comps/image/sagas/images'))

const withConnect = connect(
  (state, props) => {
    const images = selectImagesList(state, storage.getProject(), props.path)
    return {
      images
    }
  },
  (dispatch) => bindRoutineCreators({ listImages }, dispatch),
)

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(ImageList)
