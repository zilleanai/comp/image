import React from 'react'
import Helmet from 'react-helmet'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { bindRoutineCreators } from 'actions'
import { PageContent } from 'components'
import { v1 } from 'api'
import { storage } from 'comps/project'
import { FileUpload, ImageList, Dir } from 'comps/image/components'
import { DirList } from 'comps/file/components'
import { TagSelect } from 'comps/tag/components'

function image(uri) {
  return v1(`/image${uri}`)
}

class ImageUpload extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      tags: [],
    }
  }

  dirDecorator = (dir, path, i) => {
    return (
      <li key={i}>
        <Dir path={path} dir={dir} />
      </li>
    )
  }

  handleTags = ({ value: tags }) => {
    this.setState({ tags });
  }

  render() {
    const { subpath } = this.props
    const { tags } = this.state
    return (
      <PageContent>
        <Helmet>
          <title>Image Upload</title>
        </Helmet>
        <h1>Image Upload</h1>
        <label>Tags</label>
        <TagSelect onChange={this.handleTags} />
        <FileUpload url={image(`/upload`)} path={subpath} tags={tags} />
        <DirList path={subpath} decorator={this.dirDecorator} />
        <ImageList path={subpath} />
        <a href={image(`/download/${storage.getProject()}/`)} download={`${storage.getProject()}_images.zip`}><button>Download!</button></a>

      </PageContent>
    )
  }
}

const withConnect = connect(
  (state, props) => {
    const subpath = decodeURIComponent(props.match.params.subpath)
    return {
      subpath
    }
  },
  (dispatch) => bindRoutineCreators({}, dispatch),
)

export default compose(
  withConnect
)(ImageUpload)
