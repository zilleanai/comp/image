import { listImages } from 'comps/image/actions'


export const KEY = 'images'

const initialState = {
  isLoading: false,
  isLoaded: false,
  paths: [],
  byPath: {},
  error: null,
}

export default function (state = initialState, action) {
  const { type, payload } = action
  const { images } = payload || {}
  const { paths, byPath } = state

  switch (type) {
    case listImages.REQUEST:
      return {
        ...state,
        isLoading: true,
      }

    case listImages.SUCCESS:
      const path = images.project + (images.path == '' ? '/' : '/' + images.path)
      if (!paths.includes(path)) {
        paths.push(path)
      }
      byPath[path] = images.files
      return {
        ...state,
        images: images.files,
        paths,
        byPath,
        isLoaded: true,
      }

    case listImages.FAILURE:
      return {
        ...state,
        error: payload.error,
      }

    case listImages.FULFILL:
      return {
        ...state,
        isLoading: false,
      }

    default:
      return state
  }
}

export const selectImages = (state) => state[KEY]
export const selectImagesList = (state, project, path) => {
  const p = project + path
  return selectImages(state).byPath[p]
}