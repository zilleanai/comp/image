import { call, put, select, takeEvery, takeLatest } from 'redux-saga/effects'

import { createRoutineSaga } from 'sagas'
import { convertDates } from 'utils'

import { listImages } from 'comps/image/actions'
import ImageApi from 'comps/image/api'
import { selectImages } from 'comps/image/reducers/images'


export const KEY = 'images'

export const maybeListImagesSaga = function* (payload) {
  const { byPath, isLoading } = yield select(selectImages)
  const path = payload.platform + payload.path
  const isLoaded = !!byPath[path]
  if (!(isLoaded || isLoading)) {
    yield put(listImages.trigger(payload))
  }
}

export const listImagesSaga = createRoutineSaga(
  listImages,
  function* successGenerator({ payload: payload }) {
    const images = yield call(ImageApi.listImages, payload)
    yield put(listImages.success({
      images,
    }))
  },
)

export default () => [
  takeEvery(listImages.MAYBE_TRIGGER, maybeListImagesSaga),
  takeLatest(listImages.TRIGGER, listImagesSaga),
]
