import requests
from io import BytesIO
from PIL import Image


class image():
    def __init__(self, project={'api_root': ''}):
        self.project = project

    def list(self, path: str = ''):
        if not path.startswith('/'):
            path = '/'+path
        url = self.project['api_root'] + 'image/list/' + \
            str(self.project['name']) + path
        return requests.get(url).json()['files']

    def download(self, file_name: str):
        if not file_name.startswith('/'):
            file_name = '/'+file_name
        url = self.project['api_root'] + 'image/download/' + \
            str(self.project['name']) + file_name
        response = requests.get(url)
        img = Image.open(BytesIO(response.content))
        return img

    def thumbnail(self, file_name: str):
        if not file_name.startswith('/'):
            file_name = '/'+file_name
        url = self.project['api_root'] + 'image/thumbnail/' + \
            str(self.project['name']) + file_name
        response = requests.get(url)
        img = Image.open(BytesIO(response.content))
        return img
