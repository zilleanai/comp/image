from flask_unchained import (controller, resource, func, include, prefix,
                             get, delete, post, patch, put, rule)

from .views import ImageController


routes = lambda: [
    prefix('/api/v1', [
        prefix('/image', [
            controller(ImageController)
        ])
    ])
]
