# source: https://stackoverflow.com/questions/23718236/python-flask-browsing-through-directory-with-files
# https://stackoverflow.com/questions/27337013/how-to-send-zip-files-in-the-python-flask-framework

import os
import json
from flask import current_app, make_response, request, jsonify, abort, send_file
from flask_unchained import BundleConfig, Controller, route, injectable
from backend.config import Config as AppConfig
from io import StringIO, BytesIO
import zipfile
from PIL import Image
from werkzeug.utils import secure_filename

from ...tag.models import Tag
from ...project.models import Project
from bundles.file.views import FileController

from bundles.project.services import ProjectManager
from bundles.tag.services import TagManager
from bundles.file.services import FileManager
from bundles.file.db import FileDB

ALLOWED_EXTENSIONS = set(['.png', '.jpg', '.jpeg', '.tif'])


def allowed_file(filename):
    ext = os.path.splitext(filename)[1].lower()
    return '.' in filename and ext in ALLOWED_EXTENSIONS


class ImageController(Controller):

    file_manager: FileManager = injectable
    project_manager: ProjectManager = injectable
    tag_manager: TagManager = injectable

    @route('/list/<string:project>', defaults={'req_path': ''})
    @route('/list/<string:project>/<path:req_path>')
    def image_listing(self, project, req_path):
        BASE_DIR = os.path.join(AppConfig.DATA_FOLDER, project)
        os.makedirs(BASE_DIR, exist_ok=True)
        print(project, req_path)
        # Joining the base and the requested path
        abs_path = os.path.join(BASE_DIR, req_path)

        # Return 404 if path doesn't exist
        if not os.path.exists(abs_path):
            return abort(404)

        # Check if path is a file and serve
        if os.path.isfile(abs_path):
            resp = jsonify(project=project,
                           path=req_path)
        else:
            # Show directory contents
            files = list(
                filter(lambda f: allowed_file(f), os.listdir(abs_path)))
            resp = jsonify(project=project,
                           path=req_path, files=files)
        return resp

    @route('/download/<string:project>', defaults={'req_path': ''})
    @route('/download/<string:project>/<path:req_path>')
    def image_download(self, project, req_path):
        fc = FileController()
        return fc.download(project, req_path)

    @route('/thumbnail/<string:project>/<path:req_path>')
    def image_thumbnail(self, project, req_path):
        BASE_DIR = os.path.join(AppConfig.DATA_FOLDER, project)
        abs_path = os.path.join(BASE_DIR, req_path)

        if not os.path.exists(abs_path):
            return abort(404)
        img_io = BytesIO()
        img = Image.open(abs_path)
        img.thumbnail((128, 128), Image.ANTIALIAS)
        img = img.convert("RGB")
        img.save(img_io, 'JPEG', quality=70)
        img_io.seek(0)
        return send_file(img_io, mimetype='image/jpeg')

    @route('/upload/<string:project>/<string:tags>/', defaults={'req_path': ''}, methods=['GET', 'POST'])
    @route('/upload/<string:project>/<path:req_path>/<string:tags>/', methods=['GET', 'POST'])
    def image_upload(self, project, req_path, tags):
        if 'filepond' not in request.files:
            return abort(405)
        file = request.files['filepond']
        if file.filename == '':
            return abort(405)
        if file and allowed_file(file.filename):
            fc = FileController()
            return fc.upload(project, req_path, tags)
        return abort(404)
